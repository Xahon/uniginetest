#include <random>
#include <utility>

#include "Math/CircularSector.hpp"

#include "BSP/Quadtree.hpp"

#include "Game/GameField.hpp"
#include "Game/ViewArea.hpp"
#include "Game/Unit.hpp"

int main()
{
#ifdef RUN_STRESS_TEST
    float minRand = -100;
    float maxRand = 100;

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<float> dist(minRand, maxRand);

    ViewArea viewArea(135.5f, 2);
    GameField gameField(AABB{ minRand, maxRand, minRand, maxRand }, viewArea);

    for (int i = 0; i < 10000; ++i)
        gameField.addUnit(std::make_shared<Unit>(i, Vector2(dist(mt), dist(mt)), Vector2(dist(mt), dist(mt))));

    for (const auto& u : gameField.getUnits())
    {
        const auto listOfUnitsSeeing = gameField.getWhatUnitSees(u);
        GameField::printWhatUnitSees(*u, listOfUnitsSeeing.begin(), listOfUnitsSeeing.end());
    }
#endif
#ifdef RUN_MAIN_TEST
    ViewArea viewArea(135.5f, 2);
    GameField gameField(AABB{ -10, 10, -10, 10 }, viewArea);

    gameField.addUnit(std::make_shared<Unit>(1, Vector2(1, 1), Vector2(0, 1)));
    gameField.addUnit(std::make_shared<Unit>(2, Vector2(1, 2), Vector2(1, 0)));
    gameField.addUnit(std::make_shared<Unit>(3, Vector2(-5, -1), Vector2(0.707f, 0.707f)));

    for (const auto& u : gameField.getUnits())
    {
        const auto listOfUnitsSeeing = gameField.getWhatUnitSees(u);
        GameField::printWhatUnitSees(*u, listOfUnitsSeeing.begin(), listOfUnitsSeeing.end());
    }
#endif
    return 0;
}
