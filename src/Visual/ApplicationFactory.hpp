#pragma once

#include <memory>

#include "Application.hpp"

std::shared_ptr<Application> createApplication(const std::shared_ptr<GameField>& gameField);