#include <iostream>
#include <random>

#include "Application.hpp"
#include "ApplicationFactory.hpp"

#include "Math/AABB.hpp"

#include "Game/GameField.hpp"

static std::shared_ptr<GameField> testOver180Angle()
{
    AABB area(-4, 4, -4, 4);
    ViewArea viewArea(220.0f, 2);
    auto gameField = std::make_shared<GameField>(area, viewArea);

    int id = 0;
    gameField->addUnit(std::make_shared<Unit>(id++, Vector2(0, 0), Vector2(0, 1)));
    gameField->addUnit(std::make_shared<Unit>(id++, Vector2(0, 1), Vector2(0, 1)));

    return gameField;
}

static std::shared_ptr<GameField> testFourUnitsLookingOppositeDirection()
{
    AABB area(-4, 4, -4, 4);
    ViewArea viewArea(90.0f, 2);
    auto gameField = std::make_shared<GameField>(area, viewArea);

    int id = 0;
    gameField->addUnit(std::make_shared<Unit>(id++, Vector2(0, 0), Vector2(1, 1)));
    gameField->addUnit(std::make_shared<Unit>(id++, Vector2(1, 0), Vector2(-1, 1)));
    gameField->addUnit(std::make_shared<Unit>(id++, Vector2(0, 1), Vector2(1, -1)));
    gameField->addUnit(std::make_shared<Unit>(id++, Vector2(1, 1), Vector2(-1, -1)));

    return gameField;
}

static std::shared_ptr<GameField> stressTest()
{
    AABB area(-4, 4, -4, 4);
    ViewArea viewArea(220.0f, 2);
    auto gameField = std::make_shared<GameField>(area, viewArea);

    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_real_distribution<float> distX(area.getLeft(), area.getRight());
    std::uniform_real_distribution<float> distY(area.getBottom(), area.getTop());
    for (int i = 0; i < 10000; ++i)
        auto unit = gameField->addUnit(std::make_shared<Unit>(i, Vector2(distX(mt), distY(mt)), Vector2(distX(mt), distY(mt))));

    return gameField;
}

int main(int argc, char** argv)
{
//    auto gameField = testFourUnitsLookingOppositeDirection();
//    auto gameField = testOver180Angle();
    auto gameField = stressTest();

    std::shared_ptr<Application> app = createApplication(gameField);
    if (!app->isInitialized())
    {
        std::cerr << "App has not been initialized" << std::endl;
        return 1;
    }

    return app->exec();
}