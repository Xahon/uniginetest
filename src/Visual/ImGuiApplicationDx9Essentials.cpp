#include <d3d9.h>
#include <tchar.h>

#include <imgui.h>
#include <imgui_impl_dx9.h>
#include <imgui_impl_win32.h>

// Copied imgui stuff

LPDIRECT3D9 g_imgui_pD3D;
LPDIRECT3DDEVICE9 g_imgui_pd3dDevice;
D3DPRESENT_PARAMETERS g_imgui_d3dpp;
WNDCLASSEX m_wc;
HWND m_hwnd;

bool CreateDeviceD3D(HWND hWnd)
{
    if ((g_imgui_pD3D = Direct3DCreate9(D3D_SDK_VERSION)) == NULL)
        return false;

    // Create the D3DDevice
    ZeroMemory(&g_imgui_d3dpp, sizeof(g_imgui_d3dpp));
    g_imgui_d3dpp.Windowed = TRUE;
    g_imgui_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
    g_imgui_d3dpp.BackBufferFormat = D3DFMT_UNKNOWN; // Need to use an explicit format with alpha if needing per-pixel alpha composition.
    g_imgui_d3dpp.EnableAutoDepthStencil = TRUE;
    g_imgui_d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
    g_imgui_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;           // Present with vsync
    //g_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;   // Present without vsync, maximum unthrottled framerate
    if (g_imgui_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING, &g_imgui_d3dpp, &g_imgui_pd3dDevice) < 0)
        return false;

    return true;
}

void CleanupDeviceD3D()
{
    if (g_imgui_pd3dDevice)
    {
        g_imgui_pd3dDevice->Release();
        g_imgui_pd3dDevice = NULL;
    }
    if (g_imgui_pD3D)
    {
        g_imgui_pD3D->Release();
        g_imgui_pD3D = NULL;
    }
}

void ResetDevice()
{
    ImGui_ImplDX9_InvalidateDeviceObjects();
    HRESULT hr = g_imgui_pd3dDevice->Reset(&g_imgui_d3dpp);
    if (hr == D3DERR_INVALIDCALL)
        IM_ASSERT(0);
    ImGui_ImplDX9_CreateDeviceObjects();
}

// Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

// Win32 message handler
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam))
        return true;

    switch (msg)
    {
        case WM_SIZE:
            if (g_imgui_pd3dDevice != NULL && wParam != SIZE_MINIMIZED)
            {
                g_imgui_d3dpp.BackBufferWidth = LOWORD(lParam);
                g_imgui_d3dpp.BackBufferHeight = HIWORD(lParam);
                ResetDevice();
            }
            return 0;
        case WM_SYSCOMMAND:
            if ((wParam & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
                return 0;
            break;
        case WM_DESTROY:
            ::PostQuitMessage(0);
            return 0;
    }
    return ::DefWindowProc(hWnd, msg, wParam, lParam);
}

bool Imgui_Init()
{
    // Create application window
    //ImGui_ImplWin32_EnableDpiAwareness();
    m_wc = { sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, _T("Unigine Test Visualizer"), NULL };
    ::RegisterClassEx(&m_wc);
    m_hwnd = ::CreateWindow(m_wc.lpszClassName, _T("Unigine Test Visualizer (DX9)"), WS_OVERLAPPEDWINDOW, 400, 400, 1000, 700, NULL, NULL, m_wc.hInstance, NULL);

    // Initialize Direct3D
    if (!CreateDeviceD3D(m_hwnd))
    {
        CleanupDeviceD3D();
        ::UnregisterClass(m_wc.lpszClassName, m_wc.hInstance);
        return false;
    }

    // Show the window
    ::ShowWindow(m_hwnd, SW_SHOWDEFAULT);
    ::UpdateWindow(m_hwnd);

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    (void) io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer backends
    ImGui_ImplWin32_Init(m_hwnd);
    ImGui_ImplDX9_Init(g_imgui_pd3dDevice);
    return true;
}

void Imgui_Free()
{
    ImGui_ImplDX9_Shutdown();
    ImGui_ImplWin32_Shutdown();
    ImGui::DestroyContext();

    CleanupDeviceD3D();
    ::DestroyWindow(m_hwnd);
    ::UnregisterClass(m_wc.lpszClassName, m_wc.hInstance);
}

bool Imgui_StartFrame()
{
    // Poll and handle messages (inputs, window resize, etc.)
    // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
    // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
    // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
    // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
    MSG msg;
    bool done = false;
    while (::PeekMessage(&msg, nullptr, 0U, 0U, PM_REMOVE))
    {
        ::TranslateMessage(&msg);
        ::DispatchMessage(&msg);
        if (msg.message == WM_QUIT)
            done = true;
    }
    if (done)
        return false;

    // Start the Dear ImGui frame
    ImGui_ImplDX9_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();
    return true;
}

void Imgui_EndFrame(const ImVec4& clearColor)
{
    // Rendering
    ImGui::EndFrame();
    g_imgui_pd3dDevice->SetRenderState(D3DRS_ZENABLE, FALSE);
    g_imgui_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
    g_imgui_pd3dDevice->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
    D3DCOLOR clear_col_dx = D3DCOLOR_RGBA((int) (clearColor.x * clearColor.w * 255.0f), (int) (clearColor.y * clearColor.w * 255.0f), (int) (clearColor.z * clearColor.w * 255.0f),
                                          (int) (clearColor.w * 255.0f));
    g_imgui_pd3dDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, clear_col_dx, 1.0f, 0);
    if (g_imgui_pd3dDevice->BeginScene() >= 0)
    {
        ImGui::Render();
        ImGui_ImplDX9_RenderDrawData(ImGui::GetDrawData());
        g_imgui_pd3dDevice->EndScene();
    }
    HRESULT result = g_imgui_pd3dDevice->Present(NULL, NULL, NULL, NULL);

    // Handle loss of D3D9 device
    if (result == D3DERR_DEVICELOST && g_imgui_pd3dDevice->TestCooperativeLevel() == D3DERR_DEVICENOTRESET)
        ResetDevice();
}