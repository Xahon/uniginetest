#pragma once

#include <memory>

#include "Game/GameField.hpp"

class Application
{
public:
    explicit Application(const std::shared_ptr<GameField>& gameField)
            : gameField(gameField)
    {}

    virtual ~Application() = default;

    virtual bool isInitialized() = 0;

    virtual int exec() = 0;
protected:
    std::shared_ptr<GameField> gameField;
};
