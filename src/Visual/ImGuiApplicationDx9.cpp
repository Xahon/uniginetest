#include "Application.hpp"
#include "ApplicationFactory.hpp"

#include <sstream>
#include <set>

#include <imgui.h>

#include "Math/Math.hpp"

#include "Game/ViewArea.hpp"

#undef max // Thanks for good names of global macros, Microsoft!

// Declarations of internal app to imgui communication
// Implementations are defined in ImGuiApplicationDx9Essentials.cpp
// These declarations are made to avoid pushing them globally via header
bool Imgui_Init();
void Imgui_Free();
bool Imgui_StartFrame();
void Imgui_EndFrame(const ImVec4& clearColor);

struct VirtualArea
{
    const float areaStartX;
    const float areaStartY;
    const float areaWidthSize;
    const float areaHeightSize;
    const float areaUnit;
    AABB bounds;

    explicit VirtualArea(float startX, float startY, float width, const AABB& bounds)
            : areaStartX(startX)
            , areaStartY(startY)
            , areaWidthSize(width)
            , areaHeightSize(areaWidthSize / bounds.getAspectRatio())
            , areaUnit(areaWidthSize / bounds.getWidth())
            , bounds(bounds)
    {}

    float mapX(float x)
    { return map(x, bounds.getLeft(), bounds.getRight(), 0, areaWidthSize) + areaStartX; };

    float mapY(float y)
    { return map(y, bounds.getTop(), bounds.getBottom(), 0, areaHeightSize) + areaStartY; }; // flip vertically. Top and bottom are swapped

    ImVec2 mapPosVector(const Vector2& vector)
    { return { mapX(vector.x()), mapY(vector.y()) }; };

    ImVec2 mapDirVector(const Vector2& vector)
    { return { vector.x() * areaUnit, -vector.y() * areaUnit }; };
};

class ImGuiApplication : public Application
{
public:
    explicit ImGuiApplication(const std::shared_ptr<GameField>& gameField)
            : Application(gameField)
            , m_virtualArea(10, 10, 600, gameField->getBounds())
    {
        m_isInitialized = Imgui_Init();
    }

    ~ImGuiApplication() override
    { Imgui_Free(); }

    bool isInitialized() override
    { return m_isInitialized; }

    int exec() override
    {
        ImVec4 clearColor = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);

        while (true)
        {
            if (!Imgui_StartFrame())
                break;
            onUpdate();
            Imgui_EndFrame(clearColor);
        }
        return 0;
    }

private:
    void onUpdate()
    {
        auto units = gameField->getUnits();

        drawUnitsArea();
        drawUnits(units);

        if (ImGui::Begin("Inspector"))
        {
            drawUnitsListTable(units);
            drawUtilitiesPanel();
            ImGui::End();
        }
    }

    void drawUnitsArea()
    {
        auto dl = ImGui::GetBackgroundDrawList();
        dl->AddRect(ImVec2(m_virtualArea.areaStartX, m_virtualArea.areaStartY),
                    ImVec2(m_virtualArea.areaStartX + m_virtualArea.areaWidthSize, m_virtualArea.areaStartY + m_virtualArea.areaHeightSize),
                    ImColor(0, 0, 0));
    }

    void drawUnits(const std::list<std::shared_ptr<Unit>>& units)
    {
        auto dl = ImGui::GetBackgroundDrawList();
        std::set<int> unitsIdsBeenSeen;
        for (const auto& unit : units)
        {
            bool isInspected = unitIdToInspect == unit->getId();
            if (isInspected)
            {
                drawUnitViewField(unit);
                for (const auto& u : gameField->getWhatUnitSees(unit))
                {
                    unitsIdsBeenSeen.insert(u->getId());
                    ImVec2 unitMappedPos = m_virtualArea.mapPosVector(u->getPosition());
                    ImColor color = ImColor(0, 0, 255);
                    dl->AddCircleFilled(unitMappedPos, 2.0f, color);
                }
            }

            if (unitsIdsBeenSeen.find(unit->getId()) == unitsIdsBeenSeen.cend()) // if is not seen
            {
                ImVec2 unitMappedPos = m_virtualArea.mapPosVector(unit->getPosition());
                ImColor color = isInspected ? ImColor(0, 255, 100) : ImColor(0, 0, 0);
                dl->AddCircleFilled(unitMappedPos, 2.0f, color);
            }
        }
    }

    void drawUnitViewField(const std::shared_ptr<Unit>& unit)
    {
        ImVec2 unitMappedPos = m_virtualArea.mapPosVector(unit->getPosition());

        const ViewArea& viewArea = gameField->getViewArea();
        CircularSector viewSector = viewArea.getCircularSector(unit->getLookDir());
        auto dl = ImGui::GetBackgroundDrawList();
        ImColor viewAreaColor(0, 255, 0, 100);
        float viewAreaRadius = m_virtualArea.areaUnit * viewArea.getDistance();
        dl->PathClear();
        dl->PathLineTo(unitMappedPos);

        float angle1 = -angleTo360(viewSector.getA().getAngleAntiClockwise());
        float angle2 = -angleTo360(viewSector.getB().getAngleAntiClockwise());
        if (angle1 < angle2) // fix wrap around
        {
            angle1 = angleTo360(angle1);
            angle2 = angleTo180(angle2);
        }
        if (approxEqual(viewArea.getAngle(), M_F_2PI))
        { // fix imgui crash on circular path
            dl->PathClear();
            dl->AddCircleFilled(unitMappedPos, viewAreaRadius, viewAreaColor);
        } else
        {
            dl->PathArcTo(unitMappedPos, viewAreaRadius, angle1, angle2);
            dl->PathFillConvex(viewAreaColor);
        }

        if (m_showViewSectorAABB)
        {
            auto viewSectorAabb = viewSector.getAABB() + unit->getPosition();
            dl->AddRect(m_virtualArea.mapPosVector(viewSectorAabb.getBottomLeft()),
                        m_virtualArea.mapPosVector(viewSectorAabb.getTopRight()),
                        ImColor(255, 0, 255));
        }
    }

    void drawUtilitiesPanel()
    {
        if (!ImGui::CollapsingHeader("Utilities"))
            return;

        ImGui::Checkbox("Show AABB of view sector", &m_showViewSectorAABB);

        float viewAngleSweep = rad2Deg(gameField->getViewArea().getAngle());
        if (ImGui::SliderFloat("View angle sweep (deg)", &viewAngleSweep, 0, 360, "%.2f"))
            gameField->getViewArea().setAngle(deg2Rad(viewAngleSweep));
    }

    void drawUnitsListTable(const std::list<std::shared_ptr<Unit>>& units)
    {
        static ImGuiTableFlags flags =
                ImGuiTableFlags_Resizable
                | ImGuiTableFlags_RowBg | ImGuiTableFlags_Borders | ImGuiTableFlags_NoBordersInBody
                | ImGuiTableFlags_ScrollY
                | ImGuiTableFlags_SizingFixedFit;

        if (!ImGui::CollapsingHeader("Units list"))
            return;

        if (ImGui::BeginTable("units_list_table", 6, flags))
        {
            ImGui::TableSetupColumn("ID", ImGuiTableColumnFlags_DefaultSort | ImGuiTableColumnFlags_WidthFixed | ImGuiTableColumnFlags_NoHide, 0.0f, 0);
            ImGui::TableSetupColumn("X", ImGuiTableColumnFlags_WidthStretch, 0.0f, 1);
            ImGui::TableSetupColumn("Y", ImGuiTableColumnFlags_WidthStretch, 0.0f, 2);
            ImGui::TableSetupColumn("ViewX", ImGuiTableColumnFlags_WidthStretch, 0.0f, 3);
            ImGui::TableSetupColumn("ViewY", ImGuiTableColumnFlags_WidthStretch, 0.0f, 4);
            ImGui::TableSetupColumn("Show", ImGuiTableColumnFlags_WidthStretch | ImGuiTableColumnFlags_NoSort, 0.0f, 4);

            ImGui::TableHeadersRow();

            // TODO: Add sort

            ImGuiListClipper clipper;
            clipper.Begin(units.size());
            while (clipper.Step())
            {
                for (int row_n = clipper.DisplayStart; row_n < clipper.DisplayEnd && row_n < units.size(); row_n++)
                {
                    auto it = units.cbegin();
                    std::advance(it, row_n);
                    std::shared_ptr<Unit> unit = *it;
                    ImGui::TableNextRow(ImGuiTableRowFlags_None);

                    ImGui::PushID(7 + unit->getId()); // 7 is for imgui to avoid weird behaviour when id matches column id (with button in it) and misbehaves (emits click on table header instead)

                    std::stringstream oss;
                    oss.precision(2);
                    oss << std::fixed;

                    ImGui::TableSetColumnIndex(0);
                    oss.str("");
                    oss << unit->getId();
                    ImGui::TextUnformatted(oss.str().c_str());

                    ImGui::TableSetColumnIndex(1);
                    oss.str("");
                    oss << unit->getPosition().x();
                    ImGui::TextUnformatted(oss.str().c_str());

                    ImGui::TableSetColumnIndex(2);
                    oss.str("");
                    oss << unit->getPosition().y();
                    ImGui::TextUnformatted(oss.str().c_str());

                    ImGui::TableSetColumnIndex(3);
                    oss.str("");
                    oss << unit->getLookDir().x();
                    ImGui::TextUnformatted(oss.str().c_str());

                    ImGui::TableSetColumnIndex(4);
                    oss.str("");
                    oss << unit->getLookDir().y();
                    ImGui::TextUnformatted(oss.str().c_str());

                    ImGui::TableSetColumnIndex(5);
                    if (ImGui::Button("Show"))
                    {
                        unitIdToInspect = unit->getId();
                    }

                    ImGui::PopID();
                }
            }
            ImGui::EndTable();
        }
    }

private:
    bool m_isInitialized{ false };
    VirtualArea m_virtualArea;

    size_t unitIdToInspect{ std::numeric_limits<size_t>::max() };

    bool m_showViewSectorAABB{ false };
};

std::shared_ptr<Application> createApplication(const std::shared_ptr<GameField>& gameField)
{ return std::make_shared<ImGuiApplication>(gameField); }
