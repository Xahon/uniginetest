#include "ViewArea.hpp"

#include "Math/Math.hpp"

ViewArea::ViewArea(float angleDeg, float distance)
        : m_angle(deg2Rad(angleDeg))
        , m_distance(distance)
{}

CircularSector ViewArea::getCircularSector(const Vector2& direction) const
{ return { direction * m_distance, m_angle }; }

bool ViewArea::isLookingAtPoint(const Vector2& lookDirection, const Vector2& centerPosition, const Vector2& point) const
{ return getCircularSector(lookDirection).isPointInSector(point - centerPosition); }
