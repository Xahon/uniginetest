#include "Unit.hpp"

bool Unit::isSeing(const Unit& unit, const ViewArea& viewArea) const
{
    if (this == &unit)
        return false; // skip self

    Vector2 thisToUnit = unit.getPosition() - getPosition();
    if (thisToUnit.isZero())
        return true;
    if (thisToUnit.magnitudeSqr() > viewArea.getDistance() * viewArea.getDistance())
        return false;

    return viewArea.isLookingAtPoint(getLookDir(), getPosition(), unit.getPosition());
}
