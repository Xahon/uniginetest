#pragma once

#include "Math/CircularSector.hpp"

class ViewArea
{
public:
    ViewArea(float angleDeg, float distance);

    CircularSector getCircularSector(const Vector2& direction) const;
    bool isLookingAtPoint(const Vector2& lookDirection, const Vector2& centerPosition, const Vector2& point) const;

    float getAngle() const
    { return m_angle; }

    void setAngle(float angle)
    { m_angle = angle; }

    float getDistance() const
    { return m_distance; }

    void setDistance(float distance)
    { m_distance = distance; }

private:
    float m_angle, m_distance;
};
