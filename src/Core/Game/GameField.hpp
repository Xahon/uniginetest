#pragma once

#include <iostream>
#include <list>
#include <memory>

#include "Math/AABB.hpp"

#include "BSP/Quadtree.hpp"

#include "ViewArea.hpp"
#include "Unit.hpp"

class GameField
{
public:
    GameField(const AABB& bounds, const ViewArea& viewArea);

    std::shared_ptr<Unit> addUnit(const std::shared_ptr<Unit>& unit);
    std::list<std::shared_ptr<Unit>> getWhatUnitSees(const std::shared_ptr<Unit>& unit);

    template<typename Iter>
    static void printWhatUnitSees(const Unit& mainUnit, Iter begin, Iter end)
    {
        std::cout << "Unit #" << mainUnit.getId() << " sees [";
        for (auto it = begin; it != end; ++it)
        {
            std::cout << (*it)->getId();
            if (std::next(it) != end)
                std::cout << ",";
        }
        std::cout << "]" << std::endl;
    }

    const AABB& getBounds() const
    { return m_bounds; }

    const ViewArea& getViewArea() const
    { return m_viewArea; }

    ViewArea& getViewArea()
    { return m_viewArea; }

    const std::list<std::shared_ptr<Unit>>& getUnits() const
    { return m_units; }

private:
    AABB m_bounds;
    ViewArea m_viewArea;

    Quadtree<Unit> m_quadtree;
    std::list<std::shared_ptr<Unit>> m_units;
};
