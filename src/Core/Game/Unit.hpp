#pragma once

#include <list>

#include "Math/Vector2.hpp"
#include "Math/AABB.hpp"

#include "ViewArea.hpp"

class Unit
{
public:
    Unit(int id, const Vector2& position, const Vector2& lookDir)
            : m_id(id)
            , m_position(position)
            , m_lookDir(lookDir.normalized())
    {}

    bool isSeing(const Unit& unit, const ViewArea& viewArea) const;

    int getId() const { return m_id; }
    const Vector2& getPosition() const { return m_position; }
    const Vector2& getLookDir() const { return m_lookDir; }

private:
    int m_id;
    Vector2 m_position, m_lookDir;
};