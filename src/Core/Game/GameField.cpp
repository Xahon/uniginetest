#include "GameField.hpp"

GameField::GameField(const AABB& bounds, const ViewArea& viewArea)
        : m_bounds(bounds)
        , m_viewArea(viewArea)
        , m_quadtree(bounds, 2)
{}

std::shared_ptr<Unit> GameField::addUnit(const std::shared_ptr<Unit>& unit)
{
    m_units.push_back(unit);
    m_quadtree.insert(unit, unit->getPosition());
    return m_units.back();
}

std::list<std::shared_ptr<Unit>> GameField::getWhatUnitSees(const std::shared_ptr<Unit>& unit)
{
    std::list<std::shared_ptr<Unit>> foundUnits;
    AABB aabbOfSight = m_viewArea.getCircularSector(unit->getLookDir()).getAABB() + unit->getPosition();

    std::list<std::shared_ptr<Unit>> allUnitsInArea;
    m_quadtree.search(aabbOfSight, std::back_inserter(allUnitsInArea));

    auto checkIfIsSeeingOther = [this, unit](const std::shared_ptr<Unit>& other)
    {
        bool r = unit->isSeing(*other, m_viewArea);
        return r;
    };
    auto it = std::find_if(allUnitsInArea.begin(), allUnitsInArea.end(), checkIfIsSeeingOther);
    while (it != allUnitsInArea.end())
    {
        foundUnits.push_back(*it);
        it = std::find_if(std::next(it), allUnitsInArea.end(), checkIfIsSeeingOther);
    }
    return foundUnits;
}
