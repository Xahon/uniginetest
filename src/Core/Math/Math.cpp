#include "Math.hpp"

#include <cmath>

namespace
{
const float RAD_2_DEG = 180.0f / M_F_PI;
const float DEG_2_RAD = 1.0f / RAD_2_DEG;
}

float map(float x, float oldMin, float oldMax, float min, float max)
{ return (x - oldMin) * (max - min) / (oldMax - oldMin) + min; }

float map01(float x, float oldMin, float oldMax)
{ return map(x, oldMin, oldMax, 0.0f, 1.0f); }

bool approxEqual(float v1, float v2, float tolerance)
{ return std::fabsf(v1 - v2) <= tolerance; }

bool isApproxZero(float v1, float tolerance)
{ return approxEqual(v1, 0.0f, tolerance); }

float rad2Deg(float angle)
{ return RAD_2_DEG * angle; }

float deg2Rad(float angleDeg)
{ return DEG_2_RAD * angleDeg; }

float angleTo360(float angle)
{ return std::fmodf(std::fmodf(M_F_2PI + angle, M_F_2PI), M_F_2PI); }

float angleTo180(float angle)
{ return std::fmodf(angle + static_cast<float>(M_F_PI), M_F_2PI) - M_F_PI; }

bool angleIsBetween(float angle1, float angle2, float testableAngle)
{
    angle1 = angleTo360(angle1);
    angle2 = angleTo360(angle2);
    if (approxEqual(angleDiffPos(angle1, angle2), M_F_PI))
    { // if there's a 180 angle diff, then we blindly return true
        return true;
    }
    testableAngle = angleTo360(testableAngle);
    if (std::fabsf(angle1 - angle2) >= M_F_PI) // wrap around case when testable angle is between 0 and upwards and 0 and downwards sector of a circle
        return testableAngle >= angle1 && testableAngle >= angle2 || testableAngle <= angle1 && testableAngle <= angle2;
    return testableAngle >= angle1 && testableAngle <= angle2;
}

float angleDiff(float fromAngle, float toAngle)
{
    return std::fmodf(std::fmodf(toAngle - fromAngle + M_F_PI, M_F_2PI) + M_F_2PI, M_F_2PI) - M_F_PI;
}

float angleDiffPos(float angle1, float angle2)
{
    return std::fabsf(angleDiff(angle1, angle2));
}

float angleRotateAntiClockwise(float angle, float diff)
{
    return angleTo360(angle + diff);
}