#pragma once

#include <cmath>

#define M_F_PI static_cast<float>(3.1415926535897932384626433832795028841)
#define M_F_2PI static_cast<float>(2 * M_F_PI)
#define M_F_PI_2 static_cast<float>(M_F_PI / 2)
#define M_F_PI_4 static_cast<float>(M_F_PI / 4)

float map(float x, float oldMin, float oldMax, float min, float max);
float map01(float x, float oldMin, float oldMax);

bool approxEqual(float v1, float v2, float tolerance = 1e-5);
bool isApproxZero(float v1, float tolerance = 1e-5);

float rad2Deg(float angle);
float deg2Rad(float angleDeg);

float angleTo360(float angle);
float angleTo180(float angle);
bool angleIsBetween(float angle1, float angle2, float testableAngle);
float angleDiff(float fromAngle, float toAngle);
float angleDiffPos(float angle1, float angle2);
float angleRotateAntiClockwise(float angle, float diff);
