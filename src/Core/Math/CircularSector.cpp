#include "CircularSector.hpp"

#include "Math.hpp"

CircularSector::CircularSector(const Vector2& directionWithRadius, float sweepAngle)
        : a(directionWithRadius.rotated(-sweepAngle / 2.0f))
        , b(directionWithRadius.rotated(sweepAngle / 2.0f))
        , m_sweepAngle(sweepAngle)
{}

bool CircularSector::isDirectionInSector(const Vector2& direction) const
{
    return direction.isBetween(a, b, (m_sweepAngle > M_F_PI));
}

bool CircularSector::isPointInSector(const Vector2& point) const
{
    if (!isDirectionInSector(point))
        return false;
    bool inSector = point.magnitudeSqr() <= a.magnitudeSqr();
    return inSector;
}

AABB CircularSector::getAABB() const
{
    AABB aabb{ Vector2(0, 0), Vector2(0, 0) };
    if (isApproxZero(m_sweepAngle))
        return aabb;

    aabb.extend(a);
    aabb.extend(b);

    float radius = a.magnitude();
    Vector2 n(0, radius), s(0, -radius), e(radius, 0), w(-radius, 0);
    if (isDirectionInSector(n))
        aabb.extend(n);
    if (isDirectionInSector(s))
        aabb.extend(s);
    if (isDirectionInSector(e))
        aabb.extend(e);
    if (isDirectionInSector(w))
        aabb.extend(w);
    if (m_sweepAngle >= M_F_PI) // Fix more than 180deg bounding box
        aabb.extend(a.rotated(m_sweepAngle / 2.0f));

    return aabb;
}
