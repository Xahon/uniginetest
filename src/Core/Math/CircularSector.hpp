#pragma once

#include "Vector2.hpp"
#include "AABB.hpp"

class CircularSector
{
public:
    CircularSector(const Vector2& directionWithRadius, float sweepAngle);

    bool isDirectionInSector(const Vector2& direction) const;
    bool isPointInSector(const Vector2& point) const;

    AABB getAABB() const;

    const Vector2& getA() const
    { return a; }

    const Vector2& getB() const
    { return b; }

private:
    Vector2 a, b;
    float m_sweepAngle;
};
