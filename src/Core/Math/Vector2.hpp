#pragma once

class Vector2
{
public:
    struct PolarTag
    {
    };

    explicit Vector2(float x = 0, float y = 0);
    explicit Vector2(float angleDeg, float magnitude, PolarTag);
    Vector2(const Vector2& other);
    Vector2& operator=(const Vector2& other);

    float x() const
    { return v[0]; }

    float& x()
    { return v[0]; }

    float y() const
    { return v[1]; }

    float& y()
    { return v[1]; }

    float operator[](size_t index) const
    { return v[index]; }

    float& operator[](size_t index)
    { return v[index]; }

    bool isZero() const;

    float dot(const Vector2& other) const;
    float magnitudeSqr() const;
    float magnitude() const;

    float getAngleBetweenAntiClockwise(const Vector2& other) const;
    float getAngleAntiClockwise() const;

    Vector2& rotate(float angle);
    Vector2 rotated(float angle) const;

    Vector2& normalize();
    Vector2 normalized() const;

    Vector2& scaleComponentWise(const Vector2& scaler);
    Vector2 scaledComponentWise(const Vector2& scaler) const;

    bool isBetween(const Vector2& vec1, const Vector2& vec2, bool useOuterSector = false) const;

    Vector2& operator+=(const Vector2& rhs);
    Vector2 operator+(const Vector2& rhs) const;
    Vector2& operator-=(const Vector2& rhs);
    Vector2 operator-(const Vector2& rhs) const;

    Vector2& operator+=(float scalar);
    Vector2 operator+(float scalar) const;
    Vector2& operator-=(float scalar);
    Vector2 operator-(float scalar) const;
    Vector2& operator*=(float scalar);
    Vector2 operator*(float scalar) const;

private:
    float v[2]{};
};
