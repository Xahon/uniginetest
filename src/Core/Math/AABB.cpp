#include "AABB.hpp"

#include <cmath>

AABB::AABB(float left, float right, float top, float bottom)
        : l(std::fmin(left, right))
        , r(std::fmax(left, right))
        , b(std::fmin(top, bottom))
        , t(std::fmax(top, bottom))
{}

AABB::AABB(const Vector2& p1, const Vector2& p2)
        : AABB(p1.x(), p2.x(), p1.y(), p2.y())
{}

bool AABB::contains(const Vector2& point) const
{
    return l <= point.x() && r >= point.x() && b <= point.y() && t >= point.y();
}

bool AABB::intersects(const AABB& other) const
{
    return !(other.l >= r || other.r <= l || other.b >= t || other.t <= b);
}

AABB& AABB::extend(const Vector2& point)
{
    l = std::fmin(l, point.x());
    r = std::fmax(r, point.x());
    b = std::fmin(b, point.y());
    t = std::fmax(t, point.y());
    return *this;
}

AABB AABB::extended(const Vector2& point) const
{
    AABB thisCpy = *this;
    thisCpy.extend(point);
    return thisCpy;
}

AABB& AABB::combine(const AABB& other)
{
    l = std::fmin(l, other.l);
    r = std::fmax(r, other.r);
    b = std::fmin(b, other.b);
    t = std::fmax(t, other.t);
    return *this;
}

AABB AABB::combined(const AABB& other) const
{
    AABB thisCpy = *this;
    thisCpy.combine(other);
    return thisCpy;
}

AABB& AABB::operator+=(const Vector2& rhs)
{
    l += rhs.x();
    r += rhs.x();
    b += rhs.y();
    t += rhs.y();
    return *this;
}

AABB AABB::operator+(const Vector2& rhs) const
{
    AABB thisCpy = *this;
    thisCpy += rhs;
    return thisCpy;
}

AABB& AABB::operator-=(const Vector2& rhs)
{
    l -= rhs.x();
    r -= rhs.x();
    b -= rhs.y();
    t -= rhs.y();
    return *this;
}

AABB AABB::operator-(const Vector2& rhs) const
{
    AABB thisCpy = *this;
    thisCpy -= rhs;
    return thisCpy;
}
