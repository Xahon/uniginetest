#pragma once

#include "Vector2.hpp"

class AABB
{
public:
    AABB(float left, float right, float top, float bottom);
    AABB(const Vector2& p1, const Vector2& p2);

    float getLeft() const { return l; }
    float getRight() const { return r; }
    float getBottom() const { return b; }
    float getTop() const { return t; }
    float getWidth() const { return r - l; }
    float getHeight() const { return t - b; }
    float getAspectRatio() const { return getWidth() / getHeight(); }
    Vector2 getBottomLeft() const { return Vector2(getLeft(), getBottom()); }
    Vector2 getTopRight() const { return Vector2(getRight(), getTop()); }

    bool contains(const Vector2& point) const;
    bool intersects(const AABB& other) const;

    AABB& extend(const Vector2& point);
    AABB extended(const Vector2& point) const;
    AABB& combine(const AABB& other);
    AABB combined(const AABB& other) const;

    AABB& operator+=(const Vector2& rhs);
    AABB operator+(const Vector2& rhs) const;
    AABB& operator-=(const Vector2& rhs);
    AABB operator-(const Vector2& rhs) const;

private:
    float l, r, b, t;
};
