#include "Vector2.hpp"

#include <algorithm>

#include "Math.hpp"

Vector2::Vector2(float x, float y)
        : v{ x, y }
{}

Vector2::Vector2(float angleDeg, float magnitude, PolarTag)
{
    float angle = deg2Rad(angleDeg);
    x() = cos(angle) * magnitude;
    y() = sin(angle) * magnitude;
}

Vector2::Vector2(const Vector2& other)
        : v{ other.x(), other.y() }
{}

Vector2& Vector2::operator=(const Vector2& other)
{
    x() = other.x();
    y() = other.y();
    return *this;
}

float Vector2::dot(const Vector2& other) const
{
    return x() * other.x() + y() * other.y();
}

float Vector2::magnitudeSqr() const
{
    return dot(*this);
}

float Vector2::magnitude() const
{
    return std::sqrtf(magnitudeSqr());
}

Vector2& Vector2::normalize()
{
    float magSqr = magnitudeSqr();
    if (approxEqual(magSqr, 0.0f))
        return *this;
    float mag = std::sqrtf(magSqr);
    x() /= mag;
    y() /= mag;
    return *this;
}

Vector2 Vector2::normalized() const
{
    Vector2 thisCpy = *this;
    thisCpy.normalize();
    return thisCpy;
}

Vector2& Vector2::scaleComponentWise(const Vector2& scaler)
{
    x() *= scaler.x();
    y() *= scaler.y();
    return *this;
}

Vector2 Vector2::scaledComponentWise(const Vector2& scaler) const
{
    Vector2 thisCpy = *this;
    thisCpy.scaleComponentWise(scaler);
    return thisCpy;
}

bool Vector2::isBetween(const Vector2& vec1, const Vector2& vec2, bool useOuterSector) const
{
    float thisAngle = getAngleAntiClockwise();
    if (!useOuterSector)
    {
        float angle1 = angleTo360(vec1.getAngleAntiClockwise());
        float angle2 = angleTo360(vec2.getAngleAntiClockwise());
        return angleIsBetween(angle1, angle2, thisAngle);
    }

    float fromAngle = angleTo360(vec1.getAngleAntiClockwise());
    float toAngle = angleTo360(vec2.getAngleAntiClockwise());

    if (fromAngle > toAngle)
        std::swap(fromAngle, toAngle);

    if (approxEqual(M_F_PI, angleDiffPos(fromAngle, toAngle)))
    { // if both vectors are opposite, then we blindly return true
        return true;
    }

    bool isInsideSector = angleIsBetween(fromAngle, toAngle, thisAngle);
    return !isInsideSector; // if this is inside smaller sector, then it should be not between an outer one
//    float quarterRotatedFirstVectorAngle = angleRotateAntiClockwise(fromAngle, M_F_PI_2);
//    return angleIsBetween(fromAngle, quarterRotatedFirstVectorAngle, thisAngle) ||
//           angleIsBetween(quarterRotatedFirstVectorAngle, toAngle, thisAngle);
}

Vector2& Vector2::operator+=(const Vector2& rhs)
{
    x() += rhs.x();
    y() += rhs.y();
    return *this;
}

Vector2 Vector2::operator+(const Vector2& rhs) const
{
    Vector2 thisCpy = *this;
    thisCpy += rhs;
    return thisCpy;
}

Vector2& Vector2::operator-=(const Vector2& rhs)
{
    x() -= rhs.x();
    y() -= rhs.y();
    return *this;
}

Vector2 Vector2::operator-(const Vector2& rhs) const
{
    Vector2 thisCpy = *this;
    thisCpy -= rhs;
    return thisCpy;
}

Vector2& Vector2::operator+=(float scalar)
{
    x() += scalar;
    y() += scalar;
    return *this;
}

Vector2 Vector2::operator+(float scalar) const
{
    Vector2 thisCpy = *this;
    thisCpy += scalar;
    return thisCpy;
}

Vector2& Vector2::operator-=(float scalar)
{
    x() -= scalar;
    y() -= scalar;
    return *this;
}

Vector2 Vector2::operator-(float scalar) const
{
    Vector2 thisCpy = *this;
    thisCpy -= scalar;
    return thisCpy;
}


Vector2& Vector2::operator*=(float scalar)
{
    x() *= scalar;
    y() *= scalar;
    return *this;
}

Vector2 Vector2::operator*(float scalar) const
{
    Vector2 thisCpy = *this;
    thisCpy *= scalar;
    return thisCpy;
}

float Vector2::getAngleBetweenAntiClockwise(const Vector2& other) const
{ return std::atan2(other.x() * y() - other.y() * x(), dot(other)); }

float Vector2::getAngleAntiClockwise() const
{ return getAngleBetweenAntiClockwise(Vector2(1, 0)); }

bool Vector2::isZero() const
{ return isApproxZero(magnitudeSqr()); }

Vector2& Vector2::rotate(float angle)
{
    float s = std::sinf(angle);
    float c = std::cosf(angle);
    float x2 = c * x() - s * y();
    float y2 = s * x() + c * y();
    x() = x2;
    y() = y2;
    return *this;
}

Vector2 Vector2::rotated(float angle) const
{
    Vector2 thisCpy = *this;
    thisCpy.rotate(angle);
    return thisCpy;
}

