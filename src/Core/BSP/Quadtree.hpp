#pragma once

#include <algorithm>
#include <iterator>
#include <list>
#include <memory>
#include <utility>

#include "Math/AABB.hpp"

template<typename T>
struct QuadtreeNode
{
    std::shared_ptr<T> element;
    Vector2 position;
};

template<typename T>
class Quadtree
{
public:
    Quadtree(const AABB& bounds, size_t capacity);

    template<typename OutIter>
    void search(const AABB& area, OutIter out) const;
    bool insert(const std::shared_ptr<T>& element, const Vector2& position);

    AABB getAABB() const
    { return m_bounds; }

private:
    void subdivide();

private:
    AABB m_bounds;
    size_t m_capacity;
    std::list<QuadtreeNode<T>> m_nodes;
    std::unique_ptr<Quadtree> nw, ne, sw, se;
    bool m_hasSubdivisions{ false };
};

template<typename T>
Quadtree<T>::Quadtree(const AABB& bounds, size_t capacity)
        : m_bounds(bounds)
        , m_capacity(capacity)
{}

template<typename T>
template<typename OutIter>
void Quadtree<T>::search(const AABB& area, OutIter out) const
{
    if (!area.intersects(m_bounds))
        return;

    auto checkNodeInArea = [area](const QuadtreeNode<T>& node)
    {
        bool c = area.contains(node.position);
        return c;
    };

    auto it = std::find_if(m_nodes.cbegin(), m_nodes.cend(), checkNodeInArea);
    while (it != m_nodes.cend())
    {
        out = it->element;
        it = std::find_if(std::next(it), m_nodes.cend(), checkNodeInArea);
    }

    if (m_hasSubdivisions)
    {
        ne->search(area, out);
        nw->search(area, out);
        se->search(area, out);
        sw->search(area, out);
    }
}

template<typename T>
bool Quadtree<T>::insert(const std::shared_ptr<T>& element, const Vector2& position)
{
    if (m_nodes.size() >= m_capacity && !m_hasSubdivisions)
        subdivide();

    if (!m_hasSubdivisions)
    {
        if (!m_bounds.contains(position))
            return false;
        m_nodes.push_back(QuadtreeNode<T>{ element, position });
        return true;
    }

    if (ne->insert(element, position))
        return true;
    if (nw->insert(element, position))
        return true;
    if (se->insert(element, position))
        return true;
    if (sw->insert(element, position))
        return true;
    return false;
}

template<typename T>
void Quadtree<T>::subdivide()
{
    if (m_hasSubdivisions)
        return;
    float cx = (m_bounds.getRight() + m_bounds.getLeft()) / 2.0f;
    float cy = (m_bounds.getTop() + m_bounds.getBottom()) / 2.0f;

    AABB nwAabb{
            m_bounds.getLeft(),
            cx,
            m_bounds.getTop(),
            cy
    };
    nw = std::make_unique<Quadtree<T>>(nwAabb, m_capacity);

    AABB neAabb{
            m_bounds.getRight(),
            cx,
            m_bounds.getTop(),
            cy
    };
    ne = std::make_unique<Quadtree<T>>(neAabb, m_capacity);

    AABB swAabb{
            m_bounds.getLeft(),
            cx,
            m_bounds.getBottom(),
            cy
    };
    sw = std::make_unique<Quadtree<T>>(swAabb, m_capacity);

    AABB seAabb{
            m_bounds.getRight(),
            cx,
            m_bounds.getBottom(),
            cy
    };
    se = std::make_unique<Quadtree<T>>(seAabb, m_capacity);
    m_hasSubdivisions = true;
}
